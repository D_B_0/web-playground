let c;
let points = [];
let zoff = 0;
let started = false;
let smoothness;

function setup() {
  c = createCanvas(windowWidth, windowHeight);
  colorMode(RGB);
  background(document.f.background.value);
  start();
}

function draw() {
  colorMode(RGB);
  switch (document.f.background.value) {
    case "black":
      background(0, 0.0006);
      break;
    case "white":
      background(255, 0.0006);
      break;
  }
  if (started) {
    colorMode(HSB, 1);
    for (let i = 0; i < 20; i++) {
      for (let p of points) {
        let xoff = p.x * smoothness;
        let yoff = p.y * smoothness;
        let randNoise = noise(xoff, yoff, zoff);
        let force = p5.Vector.fromAngle(map(randNoise, 0, 1, -4 * Math.PI, 4 * Math.PI));
        p.add(force);
        stroke(randNoise, 1, 1, 0.05);
        strokeWeight(1);
        point(p.x, p.y);
        if (p.x < 0 || p.x > width || p.y < 0 || p.y > height) {
          p.x = random(0, width);
          p.y = random(0, height);
        }
      }
    }
  }
}

function keyPressed() {
  console.log(key);
  if (key == "Escape") {
    change();
    reset();
  }
  else if (key == " ")
    toggleStart();
}

window.onresize = () => {
  resizeCanvas(window.innerWidth, window.innerHeight);
  colorMode(RGB);
  background(document.f.background.value);
}

function toggleStart() {
  if (!smoothness) {
    colorMode(RGB);
    background(document.f.background.value);
    setInitialPoints();
    smoothness = 0.01 / parseFloat(document.f.smoth.value);
  }
  started = !started;
  setButton(started);
}

function reset() {
  colorMode(RGB);
  background(document.f.background.value);
  setInitialPoints();
  setButton(true);
  started = true;
  smoothness = 0.01 / parseFloat(document.f.smoth.value);
}

function setButton(isplay) {
  let startB = document.querySelector("div.start");
  if (isplay) {
    startB.children[0].classList.add("fa-pause");
    startB.children[0].classList.remove("fa-play");
  } else {
    startB.children[0].classList.add("fa-play");
    startB.children[0].classList.remove("fa-pause");
  }
}

function start() {
  if (!smoothness) {
    colorMode(RGB);
    background(document.f.background.value);
    setInitialPoints();
    smoothness = 0.01 / parseFloat(document.f.smoth.value);
  }
  setButton(true);
  started = true;
}

function setInitialPoints() {
  points = [];
  for (let i = 0; i < parseInt(document.f.points.value); i++) {
    let x, y;
    if (document.f.pos.value == "rand") {
      x = random(0, width);
      y = random(0, height);
    } else {
      if (random(0, 1) < 0.5) {
        x = random(0, width);
        y = round(random(0, 1)) * height;
      } else {
        x = round(random(0, 1)) * width;
        y = random(0, height);
      }
    }
    points.push(createVector(x, y));
  }
}

function change() {
  zoff++;
}
